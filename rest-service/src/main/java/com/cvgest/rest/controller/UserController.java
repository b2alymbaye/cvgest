package com.cvgest.rest.controller;

import com.cvgest.persist.model.UserEntity;
import com.cvgest.rest.dto.UserDto;
import com.cvgest.service.UserService;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;
    private final Mapper dozerMapper;

    @Autowired
    public UserController(UserService userService, Mapper dozerMapper){
        this.userService = userService;
        this.dozerMapper = dozerMapper;
    }

    @GetMapping
    public List<UserDto> getAllUsers(){
        return userService.getAllUsers()
                .stream()
                .map(userEntity -> dozerMapper.map(userEntity, UserDto.class))
                .collect(Collectors.toList());
    }

    /**
     * Allow to register a user.
     * @param userDto userDto
     */
    @PostMapping(path = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void registerUser(@RequestBody UserDto userDto){
        userService.registerUser(dozerMapper.map(userDto, UserEntity.class));
    }

    /**
     * Allow to login a user.
     * @param email email
     * @param password password
     * @return UserDto
     */
    @PostMapping(path = "/login")
    public UserDto loginUser(@RequestParam String email, @RequestParam String password){
        return dozerMapper.map(userService.loginUser(email, password), UserDto.class);
    }


}
