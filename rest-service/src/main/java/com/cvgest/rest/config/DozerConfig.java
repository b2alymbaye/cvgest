package com.cvgest.rest.config;

import org.dozer.DozerBeanMapper;
import org.dozer.config.BeanContainer;
import org.dozer.util.DefaultClassLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DozerConfig {

    @Bean
    public DozerBeanMapper mapper() throws Exception {
        DozerBeanMapper mapper = new DozerBeanMapper();
        BeanContainer.getInstance().setClassLoader(new DefaultClassLoader(Thread.currentThread().getContextClassLoader()));
        return mapper;
    }

}
