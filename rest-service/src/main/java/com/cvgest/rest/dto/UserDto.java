package com.cvgest.rest.dto;


import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserDto {
    private String email;
    private String password;
    private ProfileDto profile;
}
