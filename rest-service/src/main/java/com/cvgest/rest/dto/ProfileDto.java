package com.cvgest.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProfileDto {
    private String photo;
    private String description;
    private String cv; // curriculum vitae
    private String ml; // motivation letter
    private InformationDto information;
}
