package com.cvgest.rest.dto;

import java.util.Date;

public class InformationDto {
    private String firstname;
    private String lastname;
    private Date birthDay;
    private int phoneNumber;

}
