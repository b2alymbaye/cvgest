package com.cvgest.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "com.cvgest")
@EntityScan(basePackages = "com.cvgest.persist.model")
@EnableJpaRepositories("com.cvgest.persist.repository")
public class CvGestApplication {
    public static void main(String[] args) {
        SpringApplication.run(CvGestApplication.class, args);
    }
}



