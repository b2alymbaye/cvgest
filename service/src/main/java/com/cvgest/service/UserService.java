package com.cvgest.service;

import com.cvgest.persist.model.UserEntity;
import com.cvgest.persist.repository.InformationRepository;
import com.cvgest.persist.repository.ProfileRepository;
import com.cvgest.persist.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    private UserRepository userRepository;
    private ProfileRepository profileRepository;
    private InformationRepository informationRepository;

    @Autowired
    public UserService(UserRepository userRepository, ProfileRepository profileRepository, InformationRepository informationRepository){
        this.userRepository = userRepository;
        this.profileRepository = profileRepository;
        this.informationRepository = informationRepository;
    }

    public List<UserEntity> getAllUsers(){

        return userRepository.findAll();
    }

    public void registerUser(UserEntity userEntity) {
        informationRepository.save(userEntity.getProfile().getInformation());
        profileRepository.save(userEntity.getProfile());
        userRepository.save(userEntity);
    }

    public UserEntity loginUser(String email, String password) {
        return userRepository.findByEmailAndPassword(email, password);
    }
}
