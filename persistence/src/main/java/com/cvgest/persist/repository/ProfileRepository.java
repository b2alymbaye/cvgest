package com.cvgest.persist.repository;

import com.cvgest.persist.model.ProfileEntity;
import com.cvgest.persist.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends JpaRepository<ProfileEntity, Long> {
}
