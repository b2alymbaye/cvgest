package com.cvgest.persist.repository;

import com.cvgest.persist.model.InformationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InformationRepository extends JpaRepository<InformationEntity, Long> {

}
