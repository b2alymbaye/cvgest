package com.cvgest.persist.repository;

import com.cvgest.persist.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    /**
     * Find a user by email.
     * @param email email
     * @return a userEntity object
     */
    UserEntity findByEmail(String email);

    /**
     * Find a user by email and password.
     * @param email email
     * @param password password
     * @return a userEntity object
     */
    UserEntity findByEmailAndPassword(String email, String password);
}
